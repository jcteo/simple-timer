class stimer{
  constructor(){
    this.t = null;
  }
  start(){
    this.t = Date.now();
  }

  stop(){
    this.t = null;
  }

  get(){
    if(this.t === null){
      return 0;
    }
    return Date.now() - this.t;
  }

  format(time){
    if( typeof time != 'number' || time === 0 ){
      return "00:00:00";
    }
    var seconds = parseInt((time/1000));
    var miliseconds=time%1000;
    return this.format_number(seconds) + ":" + this.format_number(miliseconds);
  }

  format_number(number){
    let str = "";
    if(number < 10){
      str = "0" + number.toString();
    }else if(number >= 100){
      str = parseInt(number/10).toString();
    }else{
      str = number.toString();
    }
    return str.slice(0,2);
  }

}
module.exports = {
  "stimer": stimer
}
